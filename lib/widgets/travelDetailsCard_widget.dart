import 'package:finalproject_flutter/services/travel_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:finalproject_flutter/models/travel.dart';
import 'package:flutter/material.dart';

class TravelDetailCardWidget extends StatefulWidget {

  final Travel travel;

  const TravelDetailCardWidget({
    super.key,
    required this.travel
  });

  @override
  State<TravelDetailCardWidget> createState() => _TravelDetailCardWidget();
}

class _TravelDetailCardWidget extends State<TravelDetailCardWidget> {
  late Travel travel;
  late TravelDTO _travelDto = TravelDTO(id: 0, title: '', description: '', imageUrl: '', rate: 999999, status: false, createdDate: DateTime.now(), detailsTravel: []); // Inicialización predeterminada

  @override
  void initState() 
  {
    super.initState();
    travel = widget.travel;
    _cargaDetallesViaje();
  }

  void _cargaDetallesViaje() async {
    final servicio = TravelService();

    try 
    {
      var travelDetails = await servicio.getTravelById(travel.id);

      if(!travelDetails.isError)
      {
        setState(() {
          _travelDto = travelDetails.data!;
        });
      }
      else
      {
        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Error"),
              content: Text(travelDetails.errorMessage),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
    }
    catch (e) 
    {
      print('Error al obtener el detalle del viajes: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        decoration: BoxDecoration(
          color: CupertinoColors.white,
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [
            BoxShadow(
              color: CupertinoColors.systemGrey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: _travelDto.id == 0
        ? const Center(child: CircularProgressIndicator())
        : Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Mi viaje soñado:",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    flex: 2,
                    child: Text(
                      _travelDto.title,
                      style: const TextStyle(fontSize: 16),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  const Text(
                    "Mi itinerario:",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(width: 10),
                  Flexible(
                    child: Text(
                      _travelDto.detailsTravel[0].description,
                      style: const TextStyle(fontSize: 16),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 25,
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 25),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Text(
                      "Plazas disponibles:",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      _travelDto.detailsTravel[0].availablePlaces.toString(),
                      style: const TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}