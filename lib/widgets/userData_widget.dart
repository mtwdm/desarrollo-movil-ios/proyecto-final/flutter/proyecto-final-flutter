// ignore: file_names
import 'package:finalproject_flutter/controllers/session_controller.dart';
import 'package:finalproject_flutter/services/user_services.dart';
import 'package:flutter/cupertino.dart';
// ignore: depend_on_referenced_packages
import 'package:provider/provider.dart';

class FormUserDataWidget extends StatefulWidget {

  final TextEditingController fullnameController;
  final TextEditingController usernameController;
  final TextEditingController passwordController;
  final String action;

  const FormUserDataWidget({
    super.key, 
    required this.fullnameController, 
    required this.usernameController, 
    required this.passwordController,
    required this.action
  });

  @override
  State<FormUserDataWidget> createState() => _FormUserDataWidget();
}

class _FormUserDataWidget extends State<FormUserDataWidget> {
  int? _userId = 0;
  String _fullname = "";
  String _username = "";
  String _password = "";
  bool _isLoading = true;

  @override
  void initState() 
  {
    super.initState();
  }

  @override
  void didChangeDependencies() 
  {
    super.didChangeDependencies();
    if (widget.action == "ver") 
    {
      _userId = Provider.of<UserState>(context).userId;
      _cargarDatosUsuario(_userId!);
    }
  }

  void _cargarDatosUsuario(int id) async {
    final servicio = UserService();
    try 
    {
      var userData = await servicio.getUserById(id);
      setState(() {
        _isLoading = false;
        _fullname = userData.data!.fullname;
        _username = userData.data!.username;
        _password = userData.data!.password;

        widget.fullnameController.text = _fullname;
        widget.usernameController.text = _username;
        widget.passwordController.text = _password;
      });
    } 
    catch (e) 
    {
      print('Error al obtener los datos del usuario: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset( 
          'assets/images/logo.png',
          height: 200.0,
          width: 300.0,
          fit: BoxFit.cover,
        ),
        const SizedBox(height: 40.0),
        CupertinoTextField(
          placeholder: "Nombre",
          controller: widget.fullnameController,
          decoration: BoxDecoration(
            border: Border.all(color: CupertinoColors.inactiveGray),
            borderRadius: BorderRadius.circular(8.0),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        ),
        const SizedBox(height: 20.0),
        CupertinoTextField(
          placeholder: 'Nick de usuario',
          controller: widget.usernameController,
          decoration: BoxDecoration(
            border: Border.all(color: CupertinoColors.inactiveGray),
            borderRadius: BorderRadius.circular(8.0),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        ),
        const SizedBox(height: 20.0),
        CupertinoTextField(
          placeholder: 'Contraseña',
          controller: widget.passwordController,
          obscureText: true,
          decoration: BoxDecoration(
            border: Border.all(color: CupertinoColors.inactiveGray),
            borderRadius: BorderRadius.circular(8.0),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        ),
      ],
    );
  }
}