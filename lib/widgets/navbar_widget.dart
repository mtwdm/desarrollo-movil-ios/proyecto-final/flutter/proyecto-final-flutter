import 'package:finalproject_flutter/screens/userData_page.dart';
import 'package:finalproject_flutter/screens/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

// Widget reutilizable para crear un navbar customizado
class NavBarWidget extends StatelessWidget implements ObstructingPreferredSizeWidget {
  final String title;
  final bool showUserDataOption;
  final bool showBackOption;

  const NavBarWidget({
    Key? key, 
    required this.title,
    required this.showUserDataOption,
    required this.showBackOption,
  }) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  bool shouldFullyObstruct(BuildContext context) {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoNavigationBar(
      middle: Text(title),
      automaticallyImplyLeading: showBackOption,
      trailing: GestureDetector(
        onTap: () {
          showCupertinoModalPopup(
            context: context,
            builder: (BuildContext context) => CupertinoActionSheet(
              actions: <Widget>[
                if (showUserDataOption)
                  CupertinoActionSheetAction(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        CupertinoPageRoute(builder: (context) => const UserDataPage()),
                      );
                    },
                    child: const Text('Mis Datos'),
                  ),
                CupertinoActionSheetAction(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pushReplacement(
                      context,
                      CupertinoPageRoute(builder: (context) => const LoginPage()),
                    );
                  },
                  child: const Text('Cerrar Sesión'),
                ),
              ],
            ),
          );
        },
        child: const Icon(CupertinoIcons.bars),
      ),
    );
  }
}
