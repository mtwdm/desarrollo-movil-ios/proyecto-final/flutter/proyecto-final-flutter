import 'package:flutter/cupertino.dart';

// Widget reutilizable para crear un dialogo de alerta
class DialogAlert extends StatelessWidget {
  final String title;
  final String message;
  final String textConfirmationButton;

  const DialogAlert({
    super.key, 
    required this.title,
    required this.message,
    required this.textConfirmationButton
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        CupertinoDialogAction(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(textConfirmationButton),
        ),
      ],
    );
  }
}
