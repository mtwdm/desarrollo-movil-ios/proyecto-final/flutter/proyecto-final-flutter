import 'package:flutter/cupertino.dart';
import 'package:carousel_slider/carousel_slider.dart';

// Widget reutilizable para crear un carrusel de fotos
class ImagesCarruselWidget extends StatelessWidget {
  final List<String> imageUrls;

  const ImagesCarruselWidget({
    Key? key,
    required this.imageUrls,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: 250.0,
        viewportFraction: 1.0,
        enlargeCenterPage: false,
        autoPlay: true,
        autoPlayInterval: const Duration(seconds: 3),
        autoPlayAnimationDuration: const Duration(milliseconds: 800),
        enableInfiniteScroll: true,
        autoPlayCurve: Curves.fastOutSlowIn,
        scrollDirection: Axis.horizontal,
      ),
      items: imageUrls.map((url) {
        return Builder(
          builder: (BuildContext context) {
            return Image.network(
              url,
              fit: BoxFit.cover,
              width: MediaQuery.of(context).size.width * 0.90,
            );
          },
        );
      }).toList(),
    );
  }
}
