import 'package:finalproject_flutter/app.dart';
import 'package:finalproject_flutter/controllers/session_controller.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => UserState(),
      child: const MyApp(),
    ),
  );
}