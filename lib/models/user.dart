// Modelo base para un Usuario
class User {
  int id;
  String username;
  String password;
  String fullname;
  bool status;

  User({
    required this.id,
    required this.username,
    required this.password,
    required this.fullname,
    required this.status,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      username: json['username'],
      password: json['password'],
      fullname: json['fullname'],
      status: json['status'],
    );
  }

  factory User.fromJSON(Map<String, dynamic> json)
  {
    return User(id: json['id'], username: json['username'], password: json['password'], fullname: json['fullname'], status: json['status']);
  }
}

// Modelo DTO para un usuario cutomizado
class UserDTO{
  String username;
  String password;
  String fullname;
  bool status;

  UserDTO({
    required this.username,
    required this.password,
    required this.fullname,
    required this.status,
  });

  factory UserDTO.fromJson(Map<String, dynamic> json) {
    return UserDTO(
      username: json['username'],
      password: json['password'],
      fullname: json['fullname'],
      status: json['status'],
    );
  }

  factory UserDTO.fromJSON(Map<String, dynamic> json)
  {
    return UserDTO(username: json['username'], password: json['password'], fullname: json['fullname'], status: json['status']);
  }
}
