// Modelo base para un viaje
class Travel {
  int id;
  String title;
  String description;
  String imageurl;
  double rate;
  bool status;

  Travel({
    required this.id,
    required this.title,
    required this.description,
    required this.imageurl,
    required this.rate,
    required this.status,
  });

  factory Travel.fromJson(Map<String, dynamic> json) {
    return Travel(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      imageurl: json['imageurl'],
      rate: json['rate'],
      status: json['status'],
    );
  }
}

// Modelo DTO para un viaje cutomizado
class TravelDTO {
  final int id;
  final String title;
  final String description;
  final String imageUrl;
  final double rate;
  final bool status;
  final DateTime createdDate;
  final List<TravelDetailDTO> detailsTravel;

  TravelDTO({
    required this.id,
    required this.title,
    required this.description,
    required this.imageUrl,
    required this.rate,
    required this.status,
    required this.createdDate,
    required this.detailsTravel,
  });

  factory TravelDTO.fromJson(Map<String, dynamic> json) {
    List<dynamic> detailsJson = json['detailsTravel'] ?? [];
    List<TravelDetailDTO> details = detailsJson
        .map((detailJson) => TravelDetailDTO.fromJson(detailJson))
        .toList();

    return TravelDTO(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      imageUrl: json['imageurl'],
      rate: json['rate'].toDouble(),
      status: json['status'],
      createdDate: DateTime.parse(json['createdDate']),
      detailsTravel: details,
    );
  }
}

// Modelo DTO para un viajeDetail cutomizado
class TravelDetailDTO {
  final int id;
  final String description;
  final int availablePlaces;
  final bool status;
  final DateTime createdDate;
  final int travelId;

  TravelDetailDTO({
    required this.id,
    required this.description,
    required this.availablePlaces,
    required this.status,
    required this.createdDate,
    required this.travelId,
  });

  factory TravelDetailDTO.fromJson(Map<String, dynamic> json) {
    return TravelDetailDTO(
      id: json['id'],
      description: json['description'],
      availablePlaces: json['availableplaces'],
      status: json['status'],
      createdDate: DateTime.parse(json['createdDate']),
      travelId: json['travel']['id'],
    );
  }
}

// Modelo para un viajeDetailPhotos convencional
class TravelDetailsPhotos {
  int id;
  String urlimage;
  bool status;

  TravelDetailsPhotos({
    required this.id,
    required this.urlimage,
    required this.status
  });

  factory TravelDetailsPhotos.fromJson(Map<String, dynamic> json) {
    return TravelDetailsPhotos(
      id: json['id'],
      urlimage: json['urlimage'],
      status: json['status']
    );
  }
}