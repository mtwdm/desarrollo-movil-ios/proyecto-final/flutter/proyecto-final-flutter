import 'dart:convert';
import 'package:finalproject_flutter/controllers/api_response_controller.dart';
import 'package:finalproject_flutter/models/user.dart';
import 'package:http/http.dart' as http;

class UserService{
  final String _baseUrl = 'https://seahorse-app-4b9o9.ondigitalocean.app/api';

  // Llamada para registrar usuarios
  Future<ApiResponse<User>> createUser(String username, String password, String fullname) async {
    final response = await http.post(
      Uri.parse('$_baseUrl/users'),
      body: jsonEncode(<String, String>{
        'username': username,
        'password': password,
        'fullname': fullname
      }),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if(response.statusCode == 200) 
    {
      dynamic body = jsonDecode(response.body);
      User userLogin = User.fromJSON(body);
      return ApiResponse(data: userLogin, isError: false, errorMessage: "");
    }
    else if(response.statusCode == 409) 
    {
      return ApiResponse(data: null, isError: true, errorMessage: "El nombre de usuario ya se encuentra registrado.");
    }
    else
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Los datos introducidos no son correctos. (statusCode'${response.statusCode}')");
    }
  }

  // Llamada para obtener datos de un usuarios
  Future<ApiResponse<User>> getUserById(int id) async {
    final response = await http.get(Uri.parse('$_baseUrl/users/$id'));

    if(response.statusCode == 200) 
    {
      dynamic body = jsonDecode(response.body);
      User userData = User.fromJSON(body);
      return ApiResponse(data: userData, isError: false, errorMessage: "");
    }
    else if(response.statusCode == 404) 
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Los datos del usuario solicitado, no se encuentran dispobibles, por favor vuelva a intentar.");
    }
    else
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Los datos introducidos no son correctos. (statusCode'${response.statusCode}')");
    }
  } 

  // Llamada para actualizar usuarios
  Future<ApiResponse<User>> updateUserById(User user) async {
    final userId = user.id;
    final response = await http.put(
      Uri.parse('$_baseUrl/users/$userId'),
      body: jsonEncode(<String, dynamic>{
        'id': userId,
        'username': user.username,
        'password': user.password,
        'fullname': user.fullname,
        'status': user.status
      }),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if(response.statusCode == 200) 
    {
      dynamic body = jsonDecode(response.body);
      User userLogin = User.fromJSON(body);
      return ApiResponse(data: userLogin, isError: false, errorMessage: "");
    }
    else if(response.statusCode == 404) 
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Los datos del usuario solicitado, no se encuentran dispobibles, por favor vuelva a intentar.");
    }
    else
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Los datos introducidos no son correctos. (statusCode'${response.statusCode}')");
    }
  }
}