import 'dart:convert';
import 'package:finalproject_flutter/controllers/api_response_controller.dart';
import 'package:finalproject_flutter/models/travel.dart';
import 'package:http/http.dart' as http;

class TravelService{
  final String _baseUrl = 'https://seahorse-app-4b9o9.ondigitalocean.app/api';

  // Llamada para obtener todos los viajes viajes
  Future<ApiResponse<List<Travel>>> getAllTravels() async {
    final response = await http.get(Uri.parse('$_baseUrl/travels'));

    if(response.statusCode == 200) 
    {
      List<dynamic> body = jsonDecode(response.body);
      List<Travel> travelList = body.map((dynamic item) => Travel.fromJson(item)).toList();
      return ApiResponse(data: travelList, isError: false, errorMessage: "");
    }
    else
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Ha ocurrido un imprevisto al obtener el listado de viajes. (statusCode'${response.statusCode}')");
    }
  }

  // Llamada para obtener viaje por id
  Future<ApiResponse<TravelDTO>> getTravelById(int id) async {
    final response = await http.get(Uri.parse('$_baseUrl/travels/$id'));

    if(response.statusCode == 200) 
    {
      dynamic body = jsonDecode(response.body);
      TravelDTO travel = TravelDTO.fromJson(body);
      return ApiResponse(data: travel, isError: false, errorMessage: "");
    }
    else
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Ha ocurrido un imprevisto al obtener el listado de viajes. (statusCode'${response.statusCode}')");
    }
  }

  // Llamada para obtener photos por viaje por id
  Future<ApiResponse<List<TravelDetailsPhotos>>> getTravelDetailPhotosById(int id) async {
    final response = await http.get(Uri.parse('$_baseUrl/travelDetailPhotos/$id'));

    if(response.statusCode == 200) 
    {
      List<dynamic> body = jsonDecode(response.body);
      List<TravelDetailsPhotos> travelList = body.map((dynamic item) => TravelDetailsPhotos.fromJson(item)).toList();
      return ApiResponse(data: travelList, isError: false, errorMessage: "");
    }
    else
    {
      return ApiResponse(data: null, isError: true, errorMessage: "Ha ocurrido un imprevisto al obtener el listado de viajes. (statusCode'${response.statusCode}')");
    }
  }
}