import 'dart:convert';
import 'package:finalproject_flutter/controllers/api_response_controller.dart';
import 'package:finalproject_flutter/models/user.dart';
import 'package:http/http.dart' as http;

class LoginService{
  final String _baseUrl = 'https://seahorse-app-4b9o9.ondigitalocean.app/api';

    // Llamada para intentar hacer login
    Future<ApiResponse<User>> getLoginUser(String username, String password) async {
      final response = await http.post(
        Uri.parse('$_baseUrl/auth/login'),
        body: jsonEncode(<String, String>{
          'username': username,
          'password': password,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );

      if(response.statusCode == 200) 
      {
        dynamic body = jsonDecode(response.body);
        User userLogin = User.fromJSON(body);
        return ApiResponse(data: userLogin, isError: false, errorMessage: "");
      }
      else if(response.statusCode == 401) 
      {
        return ApiResponse(data: null, isError: true, errorMessage: "Los datos introducidos no son correctos.");
      }
      else
      {
        return ApiResponse(data: null, isError: true, errorMessage: "Los datos introducidos no son correctos. (statusCode'${response.statusCode}')");
      }
  }
}