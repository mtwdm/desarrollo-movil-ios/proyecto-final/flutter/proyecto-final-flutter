// Controlador para formatear el callback de la llamada del API y poder agregar un controlador de errores.
class ApiResponse<T> {
  final T? data;
  final bool isError;
  final String errorMessage;

  ApiResponse({
    required this.data,
    required this.isError,
    required this.errorMessage,
  });
}
