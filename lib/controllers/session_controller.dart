// Controlador para validar la sesión del usuario, id activo.
import 'package:flutter/material.dart';

class UserState extends ChangeNotifier {
  int? userId;

  void setUser(int id) {
    userId = id;
    notifyListeners();
  }
}
