// Controlador para validar que el dato enviado es correcto y no viene vacio
class DataValidator<T> {
  final T? data;

  DataValidator(this.data);

  bool isNotEmpty() {
    if (data is String) 
    {
      return (data as String).isNotEmpty;
    } 
      else if (data is List) 
    {
      return (data as List).isNotEmpty;
    } 
    else if (data is Map) 
    {
      return (data as Map).isNotEmpty;
    }
    
    return data != null;
  }
}
