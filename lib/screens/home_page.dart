import 'package:finalproject_flutter/screens/login_page.dart';
import 'package:flutter/cupertino.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      title: 'My Travel App',
      home: LoginPage(),
      theme: CupertinoThemeData(
        primaryColor: CupertinoColors.systemBlue,
        brightness: Brightness.light
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
