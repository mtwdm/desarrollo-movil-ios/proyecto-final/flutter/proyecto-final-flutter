// ignore: file_names
import 'package:finalproject_flutter/controllers/dataValidate_controller.dart';
import 'package:finalproject_flutter/controllers/session_controller.dart';
import 'package:finalproject_flutter/models/user.dart';
import 'package:finalproject_flutter/services/user_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:finalproject_flutter/screens/login_page.dart';
import 'package:finalproject_flutter/widgets/navbar_widget.dart';
import 'package:finalproject_flutter/widgets/userData_widget.dart';
// ignore: depend_on_referenced_packages
import 'package:provider/provider.dart';

class UserDataPage extends StatefulWidget {
  const UserDataPage({
    super.key
  });

  @override
  State<UserDataPage> createState() => _UserDataPage();
}

class _UserDataPage extends State<UserDataPage> {
  final _fullnameController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _isLoading = true;

  @override
  void initState() 
  {
    super.initState();
  }

  // Llamada para actualizar el usuario
  void _actualizaUsuario(String action, User user) async {
    final servicio = UserService();
    try 
    {
      var updatedUserData = await servicio.updateUserById(user);
      setState(() {
        _isLoading = false;
        _fullnameController.text = updatedUserData.data!.fullname;
        _usernameController.text = updatedUserData.data!.username;
        _passwordController.text = updatedUserData.data!.password;
      });

      // ignore: use_build_context_synchronously
      Navigator.pop(context);
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(
        context,
        CupertinoPageRoute(builder: (context) => const LoginPage()),
        
      );
    } 
    catch (e) 
    {
      print('Error al actualizar el usuario: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    final buttonWidth = MediaQuery.of(context).size.width * 0.8;
    final userId = Provider.of<UserState>(context).userId;
    const buttonHeight = 44.0;

    // Genero un componente interno de la screen para confirmacion
    void showConfirmationModal(BuildContext context) {
      showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) => CupertinoActionSheet(
          title: const Text('¿Estás seguro de que deseas eliminar tu cuenta?'),
          actions: <Widget>[
            CupertinoActionSheetAction(
              onPressed: () {
                _actualizaUsuario("eliminar", User(id: userId ?? 0, username: _usernameController.text, password: _passwordController.text, fullname: _fullnameController.text, status: false));
              },
              isDestructiveAction: true,
              child: const Text('Aceptar'),
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancelar'),
          ),
        ),
      );
    }

    // Función para mostrar el modal de confirmación de actualización
    void showUpdateConfirmationModal(BuildContext context) {
      showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) => CupertinoActionSheet(
          title: const Text('¿Estás seguro de que deseas actualizar tus datos? Esta acción requiere un inicio de sesión nuevo'),
          actions: <Widget>[
            CupertinoActionSheetAction(
              onPressed: () {
                _actualizaUsuario("actualizar", User(id: userId ?? 0, username: _usernameController.text, password: _passwordController.text, fullname: _fullnameController.text, status: true));
              },
              isDestructiveAction: true,
              child: const Text('Aceptar'),
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancelar'),
          ),
        ),
      );
    }

  // Genero un mensaje de error dinamico para la screen
    void _showMesaageError(BuildContext context) 
    {
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text("Error"),
            content: const Text("No se recibió toda información necesaria para la actualización del usuario. Por favor vuelve a intentarlo."),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("Aceptar"),
              ),
            ],
          );
        },
      );
    }

    return CupertinoPageScaffold(
      navigationBar: const NavBarWidget(
        title: "Mis Datos",
        showUserDataOption: false,
        showBackOption: true,
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: CupertinoColors.white,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    FormUserDataWidget(
                      fullnameController: _fullnameController,
                      usernameController: _usernameController,
                      passwordController: _passwordController,
                      action: "ver",
                    ),
                    const SizedBox(height: 40.0),
                    SizedBox(
                      width: buttonWidth,
                      height: buttonHeight,
                      child: CupertinoButton.filled(
                        onPressed: () {
                          // Mostrar el modal de confirmación al presionar "Actualizar"
                          if(DataValidator(_fullnameController.text).isNotEmpty() && DataValidator(_usernameController.text).isNotEmpty() && DataValidator(_passwordController.text).isNotEmpty())
                          {
                            showUpdateConfirmationModal(context);
                          }
                          else
                          {
                            _showMesaageError(context);
                          }
                        },
                        child: const Text('Actualizar'),
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    SizedBox(
                      width: buttonWidth,
                      height: buttonHeight,
                      child: CupertinoButton(
                        onPressed: () {
                          // Mostrar el modal de confirmación de eliminación al presionar "Eliminar"
                          showConfirmationModal(context);
                        },
                        color: CupertinoColors.destructiveRed,
                        child: const Text('Eliminar'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}