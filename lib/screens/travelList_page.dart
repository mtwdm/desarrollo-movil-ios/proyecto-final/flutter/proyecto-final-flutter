import 'package:finalproject_flutter/controllers/session_controller.dart';
import 'package:finalproject_flutter/services/travel_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:finalproject_flutter/models/travel.dart';
import 'package:finalproject_flutter/widgets/navbar_widget.dart';
import 'package:finalproject_flutter/widgets/travelCard_widget.dart';

class TravelListPage extends StatefulWidget {
  const TravelListPage({super.key});

  @override
  State<TravelListPage> createState() => _TravelListPageState();
}

class _TravelListPageState extends State<TravelListPage> {

  late List<Travel> _travelList = [];
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _cargaListaViajes();
  }

  // Llamada para obtener el listado de viajes
  void _cargaListaViajes() async {
    final servicio = TravelService();

    try 
    {
      var travelList = await servicio.getAllTravels();

      if(!travelList.isError)
      {
        setState(() {
          _travelList = travelList.data!;
          _isLoading = false;
        });
      }
      else
      {
        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Error"),
              content: Text(travelList.errorMessage),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
    }
    catch (e) 
    {
      print('Error al obtener el listado de viajes: $e');
    }
  }

  // Funcion para convertir un rate entero a emoji de estrella
  String buildStarRating(int rate) {
    String stars = '';
    for (int i = 0; i < rate; i++) {
      stars += '⭐';
    }
    return stars;
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const NavBarWidget(
        title: 'Nuestros viajes disponibles',
        showUserDataOption: true,
        showBackOption: false
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.builder(
            itemCount: _travelList.length,
            itemBuilder: (context, index) {
              final travel = _travelList[index];
              return TravelCardWidget(
                travel: travel,
              );
            },
          ),
        ),
      ),
    );
  }
}