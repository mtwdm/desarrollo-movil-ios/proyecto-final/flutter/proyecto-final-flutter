import 'package:finalproject_flutter/models/travel.dart';
import 'package:finalproject_flutter/services/travel_service.dart';
import 'package:finalproject_flutter/widgets/imagesCarrusel_widget.dart';
import 'package:finalproject_flutter/widgets/navbar_widget.dart';
import 'package:finalproject_flutter/widgets/travelDetailsCard_widget.dart';
import 'package:flutter/cupertino.dart';

class TravelDetailPage extends StatefulWidget {
  final Travel travel;
  const TravelDetailPage({Key? key, required this.travel}) : super(key: key);

  @override
  State<TravelDetailPage> createState() => _TravelDetailPage();
}

class _TravelDetailPage extends State<TravelDetailPage> {
  late Travel travel;
  late TravelDTO _travelDto = TravelDTO(id: 0, title: '', description: '', imageUrl: '', rate: 999999, status: false, createdDate: DateTime.now(), detailsTravel: []); // Inicialización predeterminada
  late List<TravelDetailsPhotos> _travelDetailsPhotos = [];
  late List<String> arrPhotos = [];

  @override
  void initState() 
  {
    super.initState();
    travel = widget.travel;
    _cargaDetallesViaje();
  }

  // Llamada para obtener el cuerpo del viaje
  void _cargaDetallesViaje() async 
  {
    final servicio = TravelService();
    try 
    {
      var travelDetails = await servicio.getTravelById(travel.id);

      if(!travelDetails.isError)
      {
        setState(() {
          _travelDto = travelDetails.data!;
          _cargaDetallesViajePhotos(_travelDto.detailsTravel[0].id);
        });
      }
      else
      {
        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Error"),
              content: Text(travelDetails.errorMessage),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
    }
    catch (e) 
    {
      print('Error al obtener el detalle del viajes: $e');
    }
  }

  // Llamada para obtener las fotos ligadas al cuerpo del viaje
  void _cargaDetallesViajePhotos(int id) async 
  {
    final servicio = TravelService();
    try 
    {
      var travelDetails = await servicio.getTravelDetailPhotosById(id);

      if(!travelDetails.isError)
      {
        setState(() {
          _travelDetailsPhotos = travelDetails.data!;
          for (int i = 0; i < _travelDetailsPhotos.length; i++) {
            arrPhotos.add(_travelDetailsPhotos[i].urlimage);
          }
        });
      }
      else
      {
        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Error"),
              content: Text(travelDetails.errorMessage),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
    }
    catch (e) 
    {
      print('Error al obtener el detalle del viajes: $e');
    }
  }

  // Se separa el contenido del modal para reservar por lo extenso
  void _mostrarInstruccionesReserva(BuildContext context) {
    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: const Text("Instrucciones de reserva"),
          content: const SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text("¡Gracias por tu interés en reservar este viaje!"),
                SizedBox(height: 20),
                Text("Por favor, sigue las siguientes instrucciones para completar tu reserva:"),
                SizedBox(height: 20),
                Text("Contacto vía Correo Electrónico:"),
                Text("- Envía un correo electrónico a travel@mtwydm.com con el asunto \"Reserva de Viaje\"."),
                Text("- En el cuerpo del correo electrónico, proporciona la siguiente información:"),
                Text("  - Nombre completo:"),
                Text("  - Número de personas:"),
                Text("  - Fecha de inicio del viaje:"),
                Text("  - Cualquier requisito especial o solicitud adicional."),
                SizedBox(height: 20),
                Text("Contacto vía WhatsApp:"),
                Text("- Envía un mensaje de WhatsApp al número +5214761046940 con el mensaje \"¡Hola! Estoy interesado en reservar un viaje \"."),
                SizedBox(height: 20),
                Text("Nuestro equipo de reservas se pondrá en contacto contigo lo antes posible para confirmar tu reserva y proporcionarte más detalles sobre tu aventura."),
                Text("¡Esperamos con ansias ayudarte a planificar un viaje increíble!"),
              ],
            ),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              child: const Text('Cerrar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: NavBarWidget(
        title: travel.title,
        showUserDataOption: true,
        showBackOption: true,
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: ImagesCarruselWidget(
                  imageUrls: arrPhotos,
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                child: TravelDetailCardWidget(travel: travel),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: CupertinoButton.filled(
                  onPressed: () {
                    _mostrarInstruccionesReserva(context);
                  },
                  child: const Text('Reservar viaje'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
