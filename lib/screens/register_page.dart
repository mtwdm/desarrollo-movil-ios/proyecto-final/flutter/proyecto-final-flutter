import 'package:finalproject_flutter/controllers/dataValidate_controller.dart';
import 'package:finalproject_flutter/models/user.dart';
import 'package:finalproject_flutter/screens/login_page.dart';
import 'package:finalproject_flutter/services/user_services.dart';
import 'package:finalproject_flutter/widgets/userData_widget.dart';
import 'package:flutter/cupertino.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> { 
  final _fullnameController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  late User _userCreated;
  bool _isLoading = true;

  @override
  void initState() 
  { 
    super.initState();
  }

  // Llamada para registrar un usuario nuevo
  void _registrarNuevoUsuario(UserDTO user) async {
    final servicio = UserService();
    try 
    {
      var userCreated = await servicio.createUser(user.username, user.password, user.fullname);
      _isLoading = false;

      if(!userCreated.isError)
      {
        setState(() {
          _userCreated = userCreated.data!;
        });

        // ignore: use_build_context_synchronously
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (context) => const LoginPage()),
        );

        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Confirmación"),
              content: const Text("Usuario registrado con éxito. Por favor, inicia sesión."),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
      else
      {
        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Error"),
              content: Text(userCreated.errorMessage),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
    }
    catch (e) 
    {
      print('Error intentar registrar los datos de usuario: $e');
    }
  }

  // Creo el mensaje de error para que se pueda reutilizar en el screen
  void _showMesaageError(BuildContext context) 
    {
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text("Error"),
            content: const Text("No se recibió toda información necesaria para el registro usuario. Por favor vuelve a intentarlo."),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("Aceptar"),
              ),
            ],
          );
        },
      );
    }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Regístrate'),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: CupertinoColors.white,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    FormUserDataWidget(
                      fullnameController: _fullnameController,
                      usernameController: _usernameController,
                      passwordController: _passwordController,
                      action: "nuevo",
                    ),
                    const SizedBox(height: 40.0),
                    CupertinoButton.filled(
                      onPressed: () {
                        // Valido que los datos vengan correctos para poder agregar
                        if(DataValidator(_fullnameController.text).isNotEmpty() && DataValidator(_usernameController.text).isNotEmpty() && DataValidator(_passwordController.text).isNotEmpty())
                          {
                            _registrarNuevoUsuario(UserDTO(username: _usernameController.text, password: _passwordController.text, fullname: _fullnameController.text, status: true));
                          }
                          else
                          {
                            _showMesaageError(context);
                          }
                      },
                      child: const Text('Registrar'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}