import 'package:finalproject_flutter/controllers/dataValidate_controller.dart';
import 'package:finalproject_flutter/models/user.dart';
import 'package:finalproject_flutter/screens/register_page.dart';
import 'package:finalproject_flutter/screens/travelList_page.dart';
import 'package:finalproject_flutter/services/login_services.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';

import '../controllers/session_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  late User _userLogin;
  bool _isLoading = true;

  @override
  void initState() 
  {
    super.initState();
  }

  // Llamada para obtener y validar usuario
  void _validaDatosInicioSesion() async {
    final servicio = LoginService();
    // Obtener los datos introducidos por el usuario
    String username = _usernameController.text;
    String password = _passwordController.text;

    try 
    {
      var isError = false;

      if(!DataValidator(username).isNotEmpty() || !DataValidator(password).isNotEmpty())
      {
        isError = true;
      }
      
      var userLogin = await servicio.getLoginUser(username, password);
      _isLoading = false;

      if(!userLogin.isError && !isError)
      {
        setState(() {
          _userLogin = userLogin.data!;
        });

        // ignore: use_build_context_synchronously
        Provider.of<UserState>(context, listen: false).setUser(_userLogin.id);

        // ignore: use_build_context_synchronously
        Navigator.pushReplacement(
          context,
          CupertinoPageRoute(builder: (context) => const TravelListPage()),
        );
      }
      else
      {
        var mensajeModal = userLogin.errorMessage;
        if(isError)
        {
          mensajeModal = 'No se recibió toda información necesaria para el inicio de sesión. Por favor vuelve a intentarlo.';
        }
        // ignore: use_build_context_synchronously
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Error"),
              content: Text(mensajeModal),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Aceptar"),
                ),
              ],
            );
          },
        );
      }
    }
    catch (e) 
    {
      print('Error al obtener los datos del usuario para inicio de sesión: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Inicia sesión'),
        automaticallyImplyLeading: false,
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 30.0),
              Image.asset( 
                'assets/images/logo.png',
                height: 200.0,
                width: 300.0,
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 40.0),
              Container(
                decoration: BoxDecoration(
                  color: CupertinoColors.white,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    CupertinoTextField(
                      controller: _usernameController,
                      placeholder: 'Nick de usuario',
                      decoration: BoxDecoration(
                        border: Border.all(color: CupertinoColors.inactiveGray),
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                    ),
                    const SizedBox(height: 20.0),
                    CupertinoTextField(
                      controller: _passwordController,
                      placeholder: 'Contraseña',
                      obscureText: true,
                      decoration: BoxDecoration(
                        border: Border.all(color: CupertinoColors.inactiveGray),
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                    ),
                    const SizedBox(height: 40.0),
                    CupertinoButton.filled(
                      onPressed: _validaDatosInicioSesion,
                      child: const Text('Iniciar sesión'),
                    ),
                    const SizedBox(height: 40.0),
                    GestureDetector(
                      onTap: () {
                      },
                      child: const Text(
                        '¿No tienes cuenta?',
                        style: TextStyle(
                          color: CupertinoColors.black,
                        ),
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          CupertinoPageRoute(builder: (context) => const RegisterPage()),
                        );
                      },
                      child: const Text(
                        'Registrate aquí',
                        style: TextStyle(
                          color: CupertinoColors.activeBlue,
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}