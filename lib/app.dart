import 'package:finalproject_flutter/screens/home_page.dart';
import 'package:flutter/cupertino.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      title: 'My Travel App',
      home: HomePage(),
      theme: CupertinoThemeData(
        primaryColor: CupertinoColors.systemBlue,
        brightness: Brightness.light
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}